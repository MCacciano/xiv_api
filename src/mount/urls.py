from django.urls import path

from . import views

app_name = 'mounts'
urlpatterns = [
    path('', views.get_mounts, name='mount-index'),
    path('<int:id>/', views.mount_detail, name='mount-detail')
]
