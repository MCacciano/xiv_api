import os
import requests
from django.shortcuts import render
# from django.utils.translation import gettext as _
from .models import Mount

BASE_URL = 'https://xivapi.com'
API_KEY = os.environ['XIV_API_KEY']


def get_mounts(request):
    r = requests.get(f'{BASE_URL}/Mount?key={API_KEY}')
    res = r.json()
    mount_results = res['Results']

    serialized_mount = []

    for m in mount_results:
        if m['Name'] != '':
            Mount.objects.update_or_create(
                id=m['ID'],
                icon=m['Icon'],
                name=m['Name'],
                url=m['Url']
            )
            # mount.save()
        # Mount.objects.filter(id=m['ID']).delete()

    mounts = Mount.objects.order_by('name')
    context = {
        'mounts': mounts
    }
    # print(mounts)

    return render(request, 'mount/index.html', context)


def mount_detail(request, id):
    mount = Mount.objects.get(id=id)
    r = requests.get(f'{BASE_URL}{mount.url}?key={API_KEY}')
    res = r.json()
    vm_mount = {}

    vm_mount['description'] = res['Description']
    vm_mount['name'] = res['Name']
    vm_mount['icon_small'] = res['IconSmall']
    vm_mount['icon'] = res['Icon']

    context = {
        'mount': vm_mount
    }

    return render(request, 'mount/mount_detail.html', context)
