from django.db import models


class Mount(models.Model):
    id = models.IntegerField(primary_key=True)
    icon = models.CharField(max_length=200)
    name = models.CharField(max_length=100)
    url = models.CharField(max_length=100)

    def __str__(self):
        return self.name
