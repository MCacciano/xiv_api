import os
import requests
from django.shortcuts import render
from .models import NewsArticle


BASE_URL = 'https://xivapi.com'
API_KEY = os.environ['XIV_API_KEY']


def home_page(request):
    r_news = requests.get(f'{BASE_URL}/lodestone/news?key={API_KEY}')
    r_notices = requests.get(f'{BASE_URL}/lodestone/notices?key={API_KEY}')

    res_news = r_news.json()
    res_notices = r_notices.json()

    news_articles = []
    notices = []

    for article in res_news:
        news_articles.append({
            'banner': article['Banner'],
            'html': article['Html'],
            'time': article['Time'],
            'title': article['Title'],
            'url': article['Url']
        })

    for n in res_notices:
        notices.append({
            'time': n['Time'],
            'title': n['Title'],
            'url': n['Url']
        })

    context = {
        'news_articles': news_articles[:6],
        'notices': notices,
        'key': API_KEY
    }

    return render(request, 'home/home_page.html', context)


def search(request):
    pass
