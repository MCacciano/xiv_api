from django.db import models

# Create your models here.


class NewsArticle(models.Model):
    title = models.CharField(max_length=200)
    banner = models.CharField(max_length=100)
    html = models.TextField()
    time = models.CharField(max_length=100)
    url = models.CharField(max_length=100)

    def __str__(self):
        return self.title
